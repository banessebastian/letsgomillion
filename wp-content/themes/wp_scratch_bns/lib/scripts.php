<?php

/*******************************************************************
* In this file will be enabled the stylesheets and javascript files
*******************************************************************/

// Register the scripts using "wp_bns_scratch_scripts" function

function wp_bns_scratch_scripts() {
  if(WP_ENV === 'development') :
    $scripts = array(
      'jquery'      => '/node_modules/jquery/dist/jquery.js',
      'scripts-js'  => '/assets/js/main.js',
    );
  else:
    $scripts = array(
      'jquery'      => 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js',
      'scripts-js'  => '/assets/js/main.min.js',
    );
  endif;

  // Enqueue scripts
  wp_deregister_script('jquery');
  wp_register_script('jquery', $scripts['jquery'], array(), null, true);

  if(WP_ENV === 'development') :
    wp_enqueue_script('wp_bns_scratch_jquery', get_template_directory_uri() . $scripts['jquery'], array(), null, true); // Enqueue jquery
  else:
    wp_enqueue_script('wp_bns_scratch_jquery', $scripts['jquery'], array(), null, true); // Enqueue jquery
  endif;

  wp_enqueue_script('wp_bns_scratch_js', get_template_directory_uri() . $scripts['scripts-js'], array(), null, true); // Enqueue our general javascript file


  // Enable comment-reply.js for comments
  if(is_single() && comments_open() && get_option('thread_comments')) :
    wp_enqueue_script('comment-reply');
  endif;
}

function wp_bns_scratch_css() {
  if(WP_ENV === 'development') :
    $css = array(
      'main-css'        => '/assets/css/main.css',
      'bootstrap-css'   => '/assets/css/bootstrap.css'
    );
  else :
    $css = array(
      'main-css'    => '/assets/css/main.min.css',
      'bootstrap-css'   => 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'
    );
  endif;
  // Enqueue styles

  // Bootstrap
  if(WP_ENV === 'development') :
    wp_enqueue_style('bootstrap-css', get_template_directory_uri() . $css['bootstrap-css'], array(), '4.0.0', 'all');
  else:
    wp_enqueue_style('bootstrap-css', $css['bootstrap-css'], array(), '4.0.0', 'all');
  endif;

  // Main css file
  wp_enqueue_style('main-css', get_template_directory_uri() . $css['main-css'], array(), '1.0.0', 'all');
}

// Add an action to execute the functions above
add_action('wp_enqueue_scripts', 'wp_bns_scratch_scripts', 100); // Enable scripts function
add_action('wp_enqueue_scripts', 'wp_bns_scratch_css', 100); // Enable css function
