<?php

$wpbns_header_scripts_stylesheets_fields = array(
  'header-scripts'  =>  array(
    'id'        =>  'header_scripts',
    'title'     =>  __('Header scripts', 'wp_bns_scratch'),
    'type'      =>  'textarea',
    'sanitize'  =>  true
  ),
  'header-stylesheets'  =>  array(
    'id'        =>  'header_stylesheets',
    'title'     =>  __('Custom CSS in header', 'wp_bns_scratch'),
    'type'      =>  'textarea',
    'sanitize'  =>  true
  ),
);
