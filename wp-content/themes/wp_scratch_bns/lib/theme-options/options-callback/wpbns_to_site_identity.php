<?php

// Generate fields
$wpbns_site_identity_fields = array(
  'website_logo'  =>  array(
    'id'    =>  'website_logo_url',
    'title' =>  __('Website logo', 'wp_bns_scratch'),
    'type'  =>  'media',
    'text'			=> __( 'Upload a logo image for your website.' ),
  ),
  'website_favicon'  =>  array(
    'id'    =>  'website_favicon_url',
    'title' =>  __('Website favicon', 'wp_bns_scratch'),
    'type'  =>  'media',
    'text'			=> __( 'Upload a favicon image for website.' ),
  ),
);
