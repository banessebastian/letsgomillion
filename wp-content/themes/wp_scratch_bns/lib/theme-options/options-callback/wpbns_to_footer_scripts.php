<?php

$wpbns_footer_scripts_fields = array(
  'footer-scripts'  =>  array(
    'id'    =>  'footer_scripts',
    'title' =>  __('Footer scripts', 'wp_bns_scratch'),
    'type'  =>  'textarea',
    'text'			=> __( 'Ex: Google Analytics tracking code.' ),
  ),
);
