<?php

$wpbns_site_social_media_fields = array(
  'facebook_url'  =>  array(
    'id'    =>  'facebook_page_url',
    'title' =>  __('Facebook', 'wp_bns_scratch'),
    'type'  =>  'text',
    'text'			=> __( 'Your Facebook page url.' ),
  ),
  'twitter_url'  =>  array(
    'id'    =>  'twitter_page_url',
    'title' =>  __('Twitter', 'wp_bns_scratch'),
    'type'  =>  'text',
    'text'			=> __( 'Your Twitter page url.' ),
  ),
  'google_plus'  =>  array(
    'id'    =>  'google_plus_page_url',
    'title' =>  __('Google+', 'wp_bns_scratch'),
    'type'  =>  'text',
    'text'			=> __( 'Your Google+ page url.' ),
  ),
  'linkedin_url'  =>  array(
    'id'    =>  'linkedin_page_url',
    'title' =>  __('Linkedin', 'wp_bns_scratch'),
    'type'  =>  'text',
    'text'			=> __( 'Your Linkedin page url.' ),
  ),
  'youtube_url'  =>  array(
    'id'    =>  'youtube_page_url',
    'title' =>  __('YouTube', 'wp_bns_scratch'),
    'type'  =>  'text',
    'text'			=> __( 'Your YouTube page url.' ),
  ),
  'instagram_url'  =>  array(
    'id'    =>  'instagram_page_url',
    'title' =>  __('Instagram', 'wp_bns_scratch'),
    'type'  =>  'text',
    'text'			=> __( 'Your Instagram page url.' ),
  ),
);
