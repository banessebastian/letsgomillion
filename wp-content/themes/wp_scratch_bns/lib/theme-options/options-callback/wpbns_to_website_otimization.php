<?php

$wpbns_site_optimization_fields = array(
  'optimization-emoji-settings'  =>  array(
    'id'    =>  'optimization_emoji_settings',
    'title' =>  __('Remove support for emoji', 'wp_bns_scratch'),
    'type'  =>  'checkbox',
    'text'			=> __( 'Check this box to remove support for emoji.' ),
  ),
  'optimization-minify-html-settings'  =>  array(
    'id'    =>  'optimization_minify_html_settings',
    'title' =>  __('Minify HTML output', 'wp_bns_scratch'),
    'type'  =>  'checkbox',
    'text'			=> __( 'Check this box to minify HTML output on all pages.' ),
  ),
);
