<?php

$wpbns_site_theme_support = array(
  'theme-support-woocommerce'  =>  array(
    'id'    =>  'theme_support_woocommerce',
    'title' =>  __('Enable support for WooCommerce', 'wp_bns_scratch'),
    'type'  =>  'checkbox',
    'text'			=> __( 'Check this box if you use WooCommerce.' ),
  ),
);
