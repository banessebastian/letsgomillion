<?php

// Get callback directory path
$callback_dir = plugin_dir_path( __FILE__ ) . '/options-callback/';

// General options
include($callback_dir . 'wpbns_to_site_identity.php');
include($callback_dir . 'wpbns_to_theme_support.php');
include($callback_dir . 'wpbns_to_header_scripts_stylesheets.php');
include($callback_dir . 'wpbns_to_footer_scripts.php');
// Social Media options
include($callback_dir . 'wpbns_to_social_media.php');
// Website optimization
include($callback_dir . 'wpbns_to_website_otimization.php');
// Homepage settings
include($callback_dir . 'wpbns_to_homepage_settings.php');

// Create an array of pages
$pages = array(
  'bns-options' =>  array(
    'page_title'  => __('WP BNS Scratch theme options', 'wp_bns_scratch'),
    'menu_title'  => __('Theme options'),
    'capability'  => 'manage_options',
    'menu_slug'   => 'theme-options',
    'position'    => 2,
    'sections'		=> array(
      'site-identity' =>  array(
        'title'     =>  __('Site identity', 'wp_bns_scratch'),
        'text'			=> '<p>' . __( 'Use this section to change the website logo, favicon, etc.', 'wp_bns_scratch' ) . '</p>',
        'include'   => $callback_dir . 'wpbns_to_site_identity.php',
        'fields'    => $wpbns_site_identity_fields
      ),
      'theme-plugins-support' =>  array(
        'title'     =>  __('Add support for plugins', 'wp_bns_scratch'),
        'include'   => $callback_dir . 'wpbns_to_theme_support.php',
        'fields'    => $wpbns_site_theme_support
      ),
      'header-scripts-stylesheets' =>  array(
        'title'     =>  __('Header scripts and stylesheets', 'wp_bns_scratch'),
        'text'			=> '<p>' . __( 'Use this section to add scripts and custom CSS in header.', 'wp_bns_scratch' ) . '</p>',
        'include'   => $callback_dir . 'wpbns_to_header_scripts_stylesheets.php',
        'fields'    => $wpbns_header_scripts_stylesheets_fields
      ),
      'footer-scripts' =>  array(
        'title'     =>  __('Footer scripts', 'wp_bns_scratch'),
        'text'			=> '<p>' . __( 'Use this section to add scripts in footer. Ex: Google Analytics tracking code.', 'wp_bns_scratch' ) . '</p>',
        'include'   => $callback_dir . 'wpbns_to_footer_scripts.php',
        'fields'    => $wpbns_footer_scripts_fields
      ),
		),
  ),
  'social-media-options'	=> array(
    'parent_slug'	=> 'theme-options',
    'page_title'	=> __( 'Social media options', 'wp_bns_scratch' ),
    'sections'		=> array(
      'social-media-profiles' =>  array(
        'title'     =>  __('Social media pages', 'wp_bns_scratch'),
        'text'			=> '<p>' . __( 'Use this section to enter your social media profiles urls.', 'wp_bns_scratch' ) . '</p>',
        'include'   => $callback_dir . 'wpbns_to_social_media.php',
        'fields'    => $wpbns_site_social_media_fields
      ),
		),

  ),
  'optimization-options'	=> array(
    'parent_slug'	=> 'theme-options',
    'page_title'	=> __( 'Website Optimization', 'wp_bns_scratch' ),
    'sections'    => array(
      'website-optimization-settings' =>  array(
        'title'     =>  __('Website Optimization Settings', 'wp_bns_scratch'),
        'include'   => $callback_dir . 'wpbns_to_website_otimization.php',
        'fields'    => $wpbns_site_optimization_fields
      )
    )
  ),
  'homepage-options' => array(
    'parent_slug' => 'theme-options',
    'page_title' => __('Homepage options', 'wp_bns_scratch'),
    'sections' => array(
      'homepage-sections-settings' => array(
        'title' => __('Homepage settings', 'wp_bns_scratch'),
        'include' => $callback_dir . 'wpbns_to_homepage_settings.php',
        'fields' => $wpbns_site_homepage_settings
      )
    )
  )
);

// Create options pages
$option_page = new RationalOptionPages($pages);
$options = get_option('bns-options');
$optimization_options = get_option('optimization-options');

function wpbns_wp_head_scripts_stylesheets()
{
  global $options;
  $header_scripts = $options['header_scripts'];
  $header_stylesheets = $options['header_stylesheets'];
  $website_favicon = $options['website_favicon_url'];

  // If we have favicon, insert favicon in header
  if(!empty($website_favicon)) :
    print_r('<link rel="shortcut icon" href="'. $website_favicon .'" type="image/x-icon"/>');
    print_r('<link rel="icon" href="'. $website_favicon .'" type="image/x-icon"/>');
  endif;

  // If we have scripts, add scripts in header
  if(!empty($header_scripts)) :
    print_r($header_scripts);
  endif;

  // Is we have custom CSS, add custom CSS in header
  if(!empty($header_stylesheets)) :
    print_r('<style type="text/css" media="all">' . $header_stylesheets . '</style>');
  endif;
}
function wpbns_wp_footer_scripts()
{
  global $options;
  $footer_scripts = $options['footer_scripts'];

  if(!empty($footer_scripts)) :
    print_r($footer_scripts);
  endif;
}
function wpbns_add_theme_features()
{
  global $options;
  global $optimization_options;

  // Add or remove support for WooCommerce
  $woocommerce_support = $options['theme_support_woocommerce'];
  if(!empty($woocommerce_support)):
    add_theme_support('woocommerce');
  endif;

  // Get optimization options
  $optimization_emoji   =  $optimization_options['optimization_emoji_settings'];
  $optimization_html    =  $optimization_options['optimization_minify_html_settings'];

  // Remove Emoji scripts and stylesheets - Remove support for emoji
  if(!empty($optimization_emoji)) :
    include('optimization/emoji.php');
  endif;

  // Minify html for page speed
  if(!empty($optimization_html)) :
    include('optimization/html-minify.php');
  endif;

}
add_action('after_setup_theme', 'wpbns_add_theme_features');
add_action('wp_head', 'wpbns_wp_head_scripts_stylesheets');
add_action('wp_footer', 'wpbns_wp_footer_scripts');
