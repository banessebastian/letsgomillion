<?php

/**********************************************************************
* Theme options. General options for this theme and it's functionality
**********************************************************************/

/* Verify WP_ENV status */

// When your website is in production mode , change 'development' with 'production', but don't forget to execute 'grunt build' before change this line.

// development - Uses the normal css and js files.
// production - Uses all minified css and js files

define('WP_ENV', 'development');

// Add support for title-tags
// http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
add_theme_support( 'title-tag' );

// Add support for HTML5 markup
// http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
add_theme_support('html5', array(
	'comment-list',
	'comment-form',
	'search-form',
	'gallery',
	'caption'
	) );

// Add support for Post_Formats
// http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Formats
add_theme_support('post-formats', array(
	'aside',
	'gallery',
	'link',
	'image',
	'quote',
	'status',
	'video',
	'audio',
	'chat'
	));

// Add support for thumbnails
// http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
add_theme_support('post-thumbnails');

// Add support for Feeds
// http://codex.wordpress.org/Function_Reference/add_theme_support#Feed_Links
add_theme_support('automatic-feed-links');

// Tell WordPress to use searchform.php from the templates/ directory
function wpbns_get_search_form() {
  $form = '';
  locate_template('/templates/searchform.php', true, false);
  return $form;
}
add_filter('get_search_form', 'wpbns_get_search_form');

/**
 * Add page slug to body_class() classes if it doesn't exist
 */
function bns_body_class($classes) {
  // Add post/page slug
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }
  return $classes;
}
add_filter('body_class', 'bns_body_class');

function remove_admin_bar(){
	return false;
}
add_filter( 'show_admin_bar' , 'remove_admin_bar');
