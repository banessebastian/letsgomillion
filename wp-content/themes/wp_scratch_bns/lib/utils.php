<?php

/**********************************************************************
* Theme utils. Register nav menus, sidebars, etc.
**********************************************************************/

// Register nav menus
if(!function_exists('bns_register_nav_menus')) :
  function bns_register_nav_menus(){
    register_nav_menus(
      array(
        'primary_menu'  =>  __('Primary menu', 'wp_bns'),
        'footer_menu'   =>  __('Footer menu', 'wp_bns')
      )
    );
  }

  add_action('after_setup_theme', 'bns_register_nav_menus');
endif;
