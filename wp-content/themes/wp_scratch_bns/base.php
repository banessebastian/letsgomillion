<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
   <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'bns_framework'); ?>
      </div>
    <![endif]-->

    <!-- Star Header -->
    <?php do_action('get_header'); get_template_part('templates/header'); ?>
    <!-- End Header -->

    <div class="wrap" role="document">
    	<div class="content">
    		<main class="main" role="main">
    			<?php include bns_template_path(); ?>
    		</main><!-- End Main -->
      <?php if (bns_display_sidebar()) : ?>
        <aside class="sidebar" role="complementary">
          <?php include bns_sidebar_path(); ?>
        </aside><!-- /.sidebar -->
      <?php endif; ?>
      </div><!-- End Content / Row -->
    </div> <!-- End Wrap / Container -->
    <?php get_template_part('templates/footer');
      wp_footer();
     ?>
</body>
</html>
