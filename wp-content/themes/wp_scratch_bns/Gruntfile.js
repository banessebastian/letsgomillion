'use strict';

module.exports = function(grunt) {

  // Load grunt tasks
  require('load-grunt-tasks')(grunt);
  // Grunt execution time
  require('time-grunt')(grunt);

  /*
  * Javscript files configuration
  */

  var jsFiles = [
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'assets/js/**/_*.js',
  ];

  /*
  * End Javscript files configuration
  */

  // Start init config for grunt
  grunt.initConfig({
    // Read package.json file
    pkg: grunt.file.readJSON('package.json'),

    // Define watch tasks
    watch: {
      styles: {
        files: [
          'assets/less/main.less',
          'assets/less/**/*.less'
        ],
        tasks:['less:development'],
        options: {
          nospawn: true
        },
      },
      scripts: {
        files: [
          'Gruntfile.js', // Watch to this file
          'assets/js/_main.js',
          'assets/js/**/*.js',
          'assets/js/**/**/*.js',
        ],
        tasks: [
          'concat'
        ],
        options: {
					spawn: false,
				},
      }
    },

    // Project less files configuration
    less: {
      development: {
        options: {
          paths: ['assets/css']
        },
        files: {
          'assets/css/main.css' : 'node_modules/bootstrap/dist/css/bootstrap.css',
          'assets/css/main.css' : 'assets/less/main.less'
        },
      }
    },

    // Project css minify configuration
    cssmin: {
      css: {
        src: 'assets/css/main.css',
        dest: 'assets/css/main.min.css'
      }
    },

    copy: {
      css: {
        expand: true,
        cwd: 'node_modules/bootstrap/dist/css/',
        src: 'bootstrap.css',
        dest: 'assets/css/'
      }
    },

    // Project js concat configuration
    concat: {
      options: {
				separator: ';',
			},
      dist: {
        src: [jsFiles],
        dest: 'assets/js/main.js',
      },
    },

    //Uglify Javascript script file
    uglify: {
      options: {
        mangle: false
      },
      js_files: {
        files: {
          'assets/js/main.min.js': 'assets/js/main.js',
        }
      }
    },

  }); // End grunt.initConfig;

  // Register basic grunt tasks

  //Default grunt task
  grunt.registerTask(
    'default',
    ['dev']
  );

  // Grunt dev task. Use this when your theme is in development mode
  grunt.registerTask(
    'dev',
    [
      'copy:css',
      'less:development',
      'concat'
    ]
  );

  // Grunt production task. Use this when your theme is ready to go on production environment.
  grunt.registerTask(
    'production',
    [
      'uglify:js_files',
      'cssmin:css',
    ]
  );

};
