<div class="homepage_slogan">
  <p>Lăsați orice precauție financiară, voi ce intrați aici.</p>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <div class="card">
      <div class="card-image-container">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/scope1.jpg" class="card-img-top" alt="Diabet">
      </div>
        <div class="card-body">
          <h5 class="card-title">Diabet</h5>
          <p class="card-text">“Viața nu se măsoară în timp ci în generoziotate.” (Peter Marshall)</p>
          <a href="#" class="btn btn-primary">Citeste mai multe...</a></a>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="card">
      <div class="card-image-container">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/scope2.jpg" class="card-img-top" alt="Dizabilități">
      </div>
        <div class="card-body">
          <h5 class="card-title">Dizabilități</h5>
          <p class="card-text">“Căutând binele altora îl găsim pe al nostru.” (Platon)</p>
          <a href="#" class="btn btn-primary">Citeste mai multe...</a>
        </div>
      </div>
    </div>
  </div>
</div>