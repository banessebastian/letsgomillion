<header class="header" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/header.png)">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
    <div class="website_logo">
      <a href="<?php echo bloginfo('url'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="<?php echo bloginfo('name'); ?>"></a>
    </div>
      <?php
        wp_nav_menu( array(
        'theme_location'    => 'primary_menu',
        'depth'             => 2,
        'container'         => 'div',
        'menu_class'        => 'navbar-nav',
        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
        'walker'            => new WP_Bootstrap_Navwalker())
        );
      ?>
    </div>
  </nav>
</header>