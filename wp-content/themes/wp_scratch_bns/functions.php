<?php

/**
 * wp_bns_scratch includes
 *
 * The $wp_bns_scratch array determines the code library included in your theme.
 */

 $wp_bns_scratch = array(
   // Classses
   'lib/classes/class.CustomPostType.php', // Create custom post types
   'lib/classes/class.CustomOptionsPage.php', // Create custom option pages
   'lib/classes/class.CustomMetaBox.php', // Create custom meta boxes
   // INC files
   'lib/wrapper.php',         // Theme Wrapper
   'lib/navigations.php',     // Theme navigation menu (Navwalker for Bootstrap)
   'lib/options.php',         // Theme general options file
   'lib/includes.php',        // Theme includes settings page
   'lib/scripts.php',         // Stylesheets and JavaScript enqueue
   'lib/utils.php',           // Use to register menus and sidebars
   'lib/sidebar.php',         // Sidebar configuration
   'lib/cpt.php',             // Register custom post types and taxonomies
    // Meta boxes
    'lib/metaboxes.php',
    // Theme options
    'lib/theme-options/theme_options.php',  // Theme options fields
    //API Server
    'lib/api.server.php'
 );

 foreach ($wp_bns_scratch as $file) {
	if (!$filepath = locate_template($file)) {
		trigger_error(sprintf(__('Error locating %s for inclusion', 'bns_framework'), $file), E_USER_ERROR);
	}

	require_once $filepath;
}
unset($file, $filepath);


// TODO: Include theme general options in code -> Logo , favicon, scripts,css in header/footer.
// TODO: Include seo settings in code.
// TODO: For Homepage SEO, modify head with new settings from file.
// TODO: For single page SEO, create metaboxes. Don't forget to apply check for another SEO plugins before.
// TODO: Include optimization settings in code.
