<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_letsgomillion');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':`7*^[eiG2Edz`().4N$ /^LPJ+c8Oxwz5]WWgwEXMIK(Ng^=ciLy #*zW<h)]Dg');
define('SECURE_AUTH_KEY',  '*TaO>N~zpQT>/DKdUE%XSY/qC,Z6VN}KY$, fStAApw5ELmcT1?.]oJy}{l>Wt#s');
define('LOGGED_IN_KEY',    '~@)CYGU#mo{6x`P?J}+JXy4QwySj3:wunG`/)A[BkO=!olxH>Q7l3@`>$6jS#.SA');
define('NONCE_KEY',        '.(uu?0`ami@[ibT_}i9nDOKHIXzRI*}dc48a,6_<G.k!.t+I?/VL]H:{Q=:ynj21');
define('AUTH_SALT',        ')p*PT9Me:0s5lvG@xl8Q3d&[gV,}OSWsQoqs~*#Q &4MZrEodyx $Kpxn8q~lK-t');
define('SECURE_AUTH_SALT', ']}?J4W9{2:PIz}MI86t/UF?8 L?`n,:ngw}SZ`xT,*pZoUdX?aI$>Tq~z*27>vq4');
define('LOGGED_IN_SALT',   'F@||nqx7_9mc;N{[wpOy;VHv74UJRa,tISRrL*=X>?blP3!MO{kP9^a29fvLUfU[');
define('NONCE_SALT',       '68@zNBj{t[)R*mmhL5pt4eKwFs1N18ohbH4}3Bh3wW+ppu];;fm^=BH!rz53`t(W');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
